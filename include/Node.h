#pragma once

#include <string>
#include <ostream>

class Node{
    std::string word;
    int amount;
    Node* right;
    Node* left;

    void setAmount(int amount);

    void incrementAmount();

    void decrementAmount();

    void setRight(Node *right);

    void setLeft(Node *left);

public:
    void setWord(const std::string &word);

private:

    friend class Tree;
public:
    Node(std::string word);

    const std::string &getWord() const;

    int getAmount() const;

    Node *getRight() const;

    Node *getLeft() const;

    bool operator==(const Node &rhs) const;

    bool operator!=(const Node &rhs) const;

    bool operator<(const Node &rhs) const;

    bool operator>(const Node &rhs) const;

    friend std::ostream &operator<<(std::ostream &os, const Node &node);
};