#include <ostream>
#include "Node.h"

class Tree{
    Node *root;

    bool insertElem(Node *&root, std::string word);

    Node* findElemRec(Node *root, std::string word);

    bool deleteElem(Node *&root, std::string word);

    void deleteElemSupport(Node *&leftTree, Node *&root);

    std::string print(std::ostream &os, Node *root) const;

    int getAmountOfWords(Node *root);

public:
    Tree();

    bool insertElem(std::string word);

    int findElemRec(std::string word);

    bool deleteElem(std::string word);

    int getAmountOfWords();

    friend std::ostream &operator<<(std::ostream &os, const Tree &tree);
};