
#include "../include/Node.h"

Node::Node(std::string word) : word(word), amount(1), right(nullptr), left(nullptr) {}

int Node::getAmount() const {
    return amount;
}

void Node::setAmount(int amount) {
    Node::amount = amount;
}

Node *Node::getRight() const {
    return right;
}

void Node::setRight(Node *right) {
    Node::right = right;
}

Node *Node::getLeft() const {
    return left;
}

void Node::setLeft(Node *left) {
    Node::left = left;
}

const std::string &Node::getWord() const {
    return word;
}

void Node::incrementAmount() {
    ++amount;
}

void Node::decrementAmount() {
    --amount;
}

bool Node::operator==(const Node &rhs) const {
    return word == rhs.word;
}

bool Node::operator!=(const Node &rhs) const {
    return !(rhs == *this);
}

bool Node::operator<(const Node &rhs) const {
    return word < rhs.word;
}

bool Node::operator>(const Node &rhs) const {
    return word > rhs.word;
}

std::ostream &operator<<(std::ostream &os, const Node &node) {
    os << node.word;
    return os;
}

void Node::setWord(const std::string &word) {
    Node::word = word;
}
