#include "../include/Tree.h"

Tree::Tree(): root(nullptr) {}

bool Tree::insertElem(Node *&root, std::string word) {
    if (!root) {
        root = new Node(word);
        return true;
    }

    if(!root->left && word < root->getWord()){
        root->left = new Node(word);
        return true;
    }

    if (root->left && word < root->getWord()) {
        return insertElem(root->left, word);
    }

    if(!root->right && word > root->getWord()){
        root->right = new Node(word);
        return true;
    }

    if (root->right && word > root->getWord()) {
        return insertElem(root->right, word);
    }

    root->incrementAmount();
    return false;
}

Node *Tree::findElemRec(Node *root, std::string word) {
    if (!root) {
        return nullptr;
    }

    if (word < root->getWord()) {
        return findElemRec(root->left, word);
    }



    if (word > root->getWord()) {
        return findElemRec(root->right, word);
    }

    return root;
}

bool Tree::insertElem(std::string word) {
    return insertElem(root, word);
}

int Tree::findElemRec(std::string word) {
    return findElemRec(root, word)->getAmount();
}

bool Tree::deleteElem(Node *&root, std::string word) {
    if(!root){
        return false;
    }

    if(!root->left && word < root->getWord()){
        return false;
    }

    if (root->left && word < root->left->getWord()) {
        return deleteElem(root->left, word);
    }

    if(!root->right && word > root->getWord()){
        return false;
    }

    if (root->right && word > root->getWord()) {
        return deleteElem(root->right, word);
    }

    if(root->getAmount() != 1){
        root->decrementAmount();
        return true;
    }

    Node *pDel = root;

    if (!root->left) {
        root = root->right;
    } else if (!root->right) {
        root = root->left;
    } else {
        deleteElemSupport(root->left, pDel);
    }

    delete pDel;
    return true;
}

void Tree::deleteElemSupport(Node *&leftTree, Node *&root) {
    if (leftTree->right) {
        deleteElemSupport(leftTree->right, root);
    }
    else
    {
        root->setWord(leftTree->getWord());
        root = leftTree;
        leftTree = leftTree->left;
    }
}

bool Tree::deleteElem(std::string word) {
    return deleteElem(root, word);
}

int Tree::getAmountOfWords(Node *root) {
    if(!root){
        return 0;
    }

    int res = root->getAmount();

    if(root->left){
        res = res + getAmountOfWords(root->left);
    }

    if(root->right){
        res = res + getAmountOfWords(root->right);
    }

    return res;
}

int Tree::getAmountOfWords() {
    return getAmountOfWords(root);
}

std::string Tree::print(std::ostream &os, Node *root) const {
    if(!root){
        return "";
    }

    if(root->left){
        os << print(os, root->left) << " ";
    }

    bool check = false;

    if(root->left || (!root->left && root->right)){
        os << root->getWord() << " ";
        check = true;
    }

    if(root->right){
        os << print(os, root->right) << " ";
    }

    if(!check){
        return root->getWord();
    }

    return "";
}

std::ostream &operator<<(std::ostream &os, const Tree &tree) {
    tree.print(os, tree.root);
    return os;
}






