#include <iostream>
#include "../include/Tree.h"

using namespace std;

int main() {
    Tree tree;
    tree.insertElem("D");
    tree.insertElem("D");
    tree.insertElem("A");
    tree.insertElem("M");
    tree.insertElem("B");
    tree.insertElem("C");
    tree.insertElem("K");
    tree.insertElem("E");
    tree.insertElem("F");
    tree.insertElem("G");
    cout << tree;
}
